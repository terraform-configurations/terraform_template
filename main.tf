provider "aws" {
  region = "ap-southeast-1"
}
resource "aws_instance" "test_instance" {
  ami           =  "ami-09a4a9ce71ff3f20b"
  instance_type = "t2.micro"
  key_name = "Terraform_test_key"

  vpc_security_group_ids = [ aws_security_group.sec_grp.id ]

  user_data = <<-EOF
              #!/bin/bash
              echo "Hello, LeanKloud" > index.html
              nohup busybox httpd -f -p "${var.server_port}" &
              EOF
  tags = {
    Name = "Terraform-test-instance"
  }
  lifecycle {
    ignore_changes = [
      user_data
    ]
  }

} 


resource "aws_security_group" "sec_grp" {
  name = "terraform-test-security-group"

  ingress {
    from_port   = 22
    to_port     = var.server_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

variable "server_port" {
  description = "The port the server will use for HTTP requests"
  type        = number
  default     = 8080
}
