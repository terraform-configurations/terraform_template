output "public_ip" {
  value       = aws_instance.test_instance.public_ip
  description = "The public IP of the web server"
}